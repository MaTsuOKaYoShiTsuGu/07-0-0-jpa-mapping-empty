package com.twuc.webApp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Office {
    @Id
    private long id;
    @Column
    private String city;

    public Office() {
    }

    public Office(long id, String city) {
        this.id = id;
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public long getId() {
        return id;
    }
}

package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class OfficeJpaTest {

    @Autowired
    private OfficeRepository or;

    @Autowired
    private EntityManager em;

    @Test
    void hello_world() {
        assertTrue(true);
    }

    @Test
    void should_save_an_entity(){
        Office office = or.save(new Office(1,"Chengdu"));
        em.flush();
        assertNotNull(office);
    }

    @Test
    void should_save_entity_with_em() {
        em.persist(new Office(1,"Chengdu"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class,1L);
        assertNotNull(office);
    }
}
